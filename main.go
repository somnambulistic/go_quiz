package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"strings"
	"time"
)

func main() {
	csvFilename := flag.String("csv", "problems.csv", "fichier csv au format 'question, reponse'")
	timeLimit := flag.Int("limit", 30, "le temps limit pour le quiz en sec")
	flag.Parse()

	file, err := os.Open(*csvFilename)
	if err != nil {
		exit(fmt.Sprintf("échec ouverture du fichier CSV: %s\n", *csvFilename))
	}

	r := csv.NewReader(file)
	lines, err := r.ReadAll()
	if err != nil {
		exit("échec de la lecture du fichier CSV")
	}

	problems := parseLines(lines)

	timer := time.NewTimer(time.Duration(*timeLimit) * time.Second)

	correct := 0

problemloop:
	for i, p := range problems {
		fmt.Printf("Problem #%d: %s = ", i+1, p.q)

		answerCh := make(chan string)

		go func() {
			var answer string
			fmt.Scanf("%s\n", &answer)
			answerCh <- answer
		}()

		select {
		case <-timer.C:
			fmt.Printf("\nTon score est de %d sur %d.\n", correct, len(problems))
			// return
			break problemloop

		case answer := <-answerCh:
			if answer == p.a {
				correct++
			}
			// default:
			// 	var answer string
			// 	fmt.Scanf("%s\n", &answer)
			// 	if answer == p.a {
			// 		//fmt.Println("Bonne réponse!!")
			// 		correct++
			// 	}
		}
	}

}

func parseLines(lines [][]string) []problem {
	ret := make([]problem, len(lines))
	for i, line := range lines {
		ret[i] = problem{
			q: line[0],
			a: strings.TrimSpace(line[1]),
		}
	}
	return ret
}

type problem struct {
	q string
	a string
}

func exit(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}
